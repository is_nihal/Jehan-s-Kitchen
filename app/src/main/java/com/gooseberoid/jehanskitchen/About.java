package com.gooseberoid.jehanskitchen;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

    public void goToAboutUs(View view)
    {
        Intent intent = new Intent(this,AboutUs.class);
        startActivity(intent);
    }

    public void goToPrivacyPolicy(View view)
    {
        Intent intent=new Intent(this,PrivacyPolicy.class);
        startActivity(intent);
    }

    public void goToFaceBookPage(View view)
    {
        Uri uri = Uri.parse("https://m.facebook.com/jehanrazdan");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.facebook.katana");

        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://m.facebook.com/jehanrazdan")));
        }

    }

    public void goToInstagramPage(View view)
    {
        Uri uri = Uri.parse("https://instagram.com/jehanrazdan");
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://instagram.com/jehanrazdan")));
        }
    }
}
