package com.gooseberoid.jehanskitchen;

import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;
import com.gooseberoid.jehanskitchen.R;


public class VideoPlayer extends AppCompatActivity {

    static String videoId;
    YouTubePlayerView youtubePlayerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        youtubePlayerView = findViewById(R.id.youtube_player_view);
        //View customPlayerUI = youtubePlayerView.inflateCustomPlayerUI(R.layout.player_layout);
        //youtubePlayerView.removeAllViews();
        getLifecycle().addObserver(youtubePlayerView);
        youtubePlayerView.enterFullScreen();

        youtubePlayerView.initialize(new YouTubePlayerInitListener() {
            @Override
            public void onInitSuccess(@NonNull final YouTubePlayer initializedYouTubePlayer) {
                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {
                        initializedYouTubePlayer.loadVideo(videoId, 0);
                    }
                });
            }
        }, true);
    }


    public static void setVideoId(String id)
    {
        videoId=id;
    }


    public static String getVideoURL()
    {
        return("https://youtu.be/"+videoId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        youtubePlayerView.release();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MainActivityTabbed.increaseNumberOfTimesVideoViewed();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        finish();
    }
}
