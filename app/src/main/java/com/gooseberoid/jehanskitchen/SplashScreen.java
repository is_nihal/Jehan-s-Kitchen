package com.gooseberoid.jehanskitchen;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.gooseberoid.jehanskitchen.R;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);


        //Edit this for setting splash screen duration
        int timeLeft=3000;


        final Intent intent=new Intent(this,MainActivityTabbed.class);

        //Start the countdown of splash screen
        new CountDownTimer(timeLeft, 1000) {

            @Override
            public void onTick(long l) {

            }


            public void onFinish() {
                startActivity(intent);
                finish();
            }

        }.start();//Start the countdown timer
    }
}
