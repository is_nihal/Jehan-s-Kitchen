package com.gooseberoid.jehanskitchen;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.prof.youtubeparser.models.videos.Video;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import com.gooseberoid.jehanskitchen.R;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.VideoViewHolder> {

    List<Video> videos;

    RVAdapter(List<Video> videos){
        this.videos = videos;
    }


    @NonNull
    @Override
    public VideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        VideoViewHolder videoViewHolder;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview, parent, false);
        videoViewHolder = new VideoViewHolder(view);
        return (videoViewHolder);

    }

    @Override
    public void onBindViewHolder(@NonNull final VideoViewHolder holder, final int position) {
        int viewType = getItemViewType(position);
        holder.titleText.setText(videos.get(position).getTitle());
        /*Picasso.get()
                .load(changeToMQdeafult(videos.get(position).getCoverLink()))
                .into(holder.titleImage);*/

        Picasso.get().load(changeToMQdeafult(videos.get(position).getCoverLink())).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                holder.titleImage.setBackground(new BitmapDrawable(bitmap));
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }


            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VideoPlayer.setVideoId(videos.get(position).getVideoId());
                v.getContext().startActivity(new
                        Intent(v.getContext(), VideoPlayer.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private String changeToMQdeafult(String original)
    {
        return (original.replace("hqdefault","mqdefault"));
    }

    public static class VideoViewHolder extends RecyclerView.ViewHolder
    {

        CardView cardView;
        ImageView playButton,titleImage;
        TextView titleText;


        public VideoViewHolder(View itemView) {
            super(itemView);
            cardView=itemView.findViewById(R.id.cardView);
            playButton=itemView.findViewById(R.id.playButtonImage);
            titleImage=itemView.findViewById(R.id.titleImage);
            titleText=itemView.findViewById(R.id.titleText);
        }
    }

}
