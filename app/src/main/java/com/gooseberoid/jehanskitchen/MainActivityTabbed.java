package com.gooseberoid.jehanskitchen;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.prof.youtubeparser.Parser;
import com.prof.youtubeparser.models.videos.Video;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.gooseberoid.jehanskitchen.R;

public class MainActivityTabbed extends AppCompatActivity implements NetworkStateReciever.NetworkStateReceiverListener {

    private NetworkStateReciever networkStateReceiver;
    RelativeLayout noNetworkLayout;
    RecyclerView recyclerView1,recyclerView2;

    String CHANNEL_ID="UCGk3OKw8YnGBbr5KUryWvog",
    //TODO:DELETE
    API_KEY="AIzaSyCqev5sgsZDjsL64E_thTgZVKTybsTwZhg";

    private static int numberOfTimesVideoViewed=1;

    private InterstitialAd mInterstitialAd;

    ArrayList<Video> videoArrayList1,videoArrayList2,videoArrayList;

    AHBottomNavigation bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tabbed);

        noNetworkLayout=findViewById(R.id.noNetworkLayout);
        recyclerView1=findViewById(R.id.recyclerView1);
        recyclerView2=findViewById(R.id.recyclerView2);

        videoArrayList=new ArrayList<>();
        videoArrayList1=new ArrayList<>();
        videoArrayList2=new ArrayList<>();

        recyclerView1.setHasFixedSize(true);
        recyclerView2.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager1=new LinearLayoutManager(this);
        recyclerView1.setLayoutManager(linearLayoutManager1);

        LinearLayoutManager linearLayoutManager2=new LinearLayoutManager(this);
        recyclerView2.setLayoutManager(linearLayoutManager2);

        networkStateReceiver = new NetworkStateReciever();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        //Making the action bar icon a circle
        ImageView actionBarIcon=findViewById(R.id.toolbar_image);
        Picasso.get()
                .load(R.drawable.jehan)
                .transform(new CircleTransform())
                .into(actionBarIcon);

        setupBottomNavigation();

        //Initalzie google ad mobs sdk
        MobileAds.initialize(this,getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_interstitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_dropdown1:
                return true;

            case R.id.action_dropdown2:
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    private void fetchContentFromYoutube()
    {
        //Youtube parser code
        final Parser parser = new Parser();
        //(CHANNEL_ID, NUMBER_OF_RESULT_TO_SHOW, ORDER_TYPE ,BROSWER_API_KEY)
        //https://www.youtube.com/channel/UCVHFbqXqoYvEWM1Ddxl0QDg --> channel id = UCVHFbqXqoYvEWM1Ddxl0QDg
        //The maximum number of result to show is 50
        //ORDER_TYPE --> by date: "Parser.ORDER_DATE" or by number of views: "ORDER_VIEW_COUNT"
        String url = parser.generateRequest(CHANNEL_ID, 50, Parser.ORDER_DATE, API_KEY);
        parser.execute(url);
        parser.onFinish(new Parser.OnTaskCompleted() {

            @Override
            public void onTaskCompleted(ArrayList<Video> list, String nextPageToken) {
                //What to do when the parsing is done
                //the ArrayList contains all video data. For example you can use it for your adapter
                videoArrayList.addAll(list);
                Log.i("AWESOME 1",list.toString());
                Parser parser2 = new Parser();
                String url2 = parser.generateMoreDataRequest(CHANNEL_ID, 50, Parser.ORDER_DATE, API_KEY, nextPageToken);
                parser2.execute(url2);
                parser2.onFinish(new Parser.OnTaskCompleted() {
                    @Override
                    public void onTaskCompleted(ArrayList<Video> list, String nextPageToken) {
                        Log.i("AWESOME 2",list.toString());
                        videoArrayList.addAll(list);
                        videoArrayList1=addVideo(0);
                        videoArrayList2=addVideo(1);
                        //SET ADAPTER
                        RVAdapter adapter = new RVAdapter(videoArrayList1);
                        RVAdapter adapter1= new RVAdapter(videoArrayList2);

                        recyclerView1.setAdapter(adapter);
                        recyclerView2.setAdapter(adapter1);
                    }

                    @Override
                    public void onError() {
                        //What to do in case of error
                    }
                });
            }

            @Override
            public void onError() {
                //What to do in case of error
            }
        });
    }

    private ArrayList<Video> addVideo(int flag) {
        ArrayList<Video> processedList = new ArrayList<>();
        if (flag == 0) {
            for (Video v : videoArrayList) {
                if (v.getTitle().contains("Jehans Kitchen") || v.getTitle().contains("Jehan's Kitchen")) {
                    processedList.add(v);
                }
            }
        } else if (flag == 1) {
            for (Video v : videoArrayList) {
                if (!(v.getTitle().contains("Jehans Kitchen") || v.getTitle().contains("Jehan's Kitchen"))) {
                    processedList.add(v);
                }
            }
        }
        return processedList;
    }


    private void setupBottomNavigation()
    {
        bottomNavigation = findViewById(R.id.bottom_navigation);
        // Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_1, R.drawable.film, R.color.primary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.coffee, R.color.primary_dark);

        // Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);

        // Set background color
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));

        // Change colors
        bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

        // Force to tint the drawable (useful for font with icon for example)
        bottomNavigation.setForceTint(true);

        bottomNavigation.setTranslucentNavigationEnabled(true);


        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

        // Use colored navigation with circle reveal effect
        bottomNavigation.setColored(true);

        // Set current item programmatically
        bottomNavigation.setCurrentItem(0);

        // Set listeners
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                // Do something cool here...
                if (position==0)
                {
                    recyclerView1.setVisibility(View.VISIBLE);
                    recyclerView2.setVisibility(View.GONE);
                }
                else if(position==1)
                {
                    recyclerView1.setVisibility(View.GONE);
                    recyclerView2.setVisibility(View.VISIBLE);
                }
                return true;
            }
        });
    }

    public void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void networkAvailable() {
        noNetworkLayout.setVisibility(View.GONE);
        // Set listeners
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                // Do something cool here...
                if (position==0)
                {
                    recyclerView1.setVisibility(View.VISIBLE);
                    recyclerView2.setVisibility(View.GONE);
                }
                else if(position==1)
                {
                    recyclerView1.setVisibility(View.GONE);
                    recyclerView2.setVisibility(View.VISIBLE);
                }
                return true;
            }
        });
        if(videoArrayList1.isEmpty())
        {
            fetchContentFromYoutube();
        }
    }

    @Override
    public void networkUnavailable() {
        noNetworkLayout.setVisibility(View.VISIBLE);
        recyclerView1.setVisibility(View.GONE);
        recyclerView2.setVisibility(View.GONE);
    }

    public static void increaseNumberOfTimesVideoViewed()
    {
        numberOfTimesVideoViewed++;
    }

    @Override
    protected void onResume() {
        if(numberOfTimesVideoViewed%4==0)
        {
            Log.i("Superman","Showing AD");
            //Show Ad
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
            numberOfTimesVideoViewed=1;
        }
        super.onResume();
    }

    public void goToAbout(View view)
    {
        Intent intent = new Intent(this,About.class);
        startActivity(intent);
    }
}
